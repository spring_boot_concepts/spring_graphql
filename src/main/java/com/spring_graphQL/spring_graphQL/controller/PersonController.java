package com.spring_graphQL.spring_graphQL.controller;


import com.spring_graphQL.spring_graphQL.dao.PersonRepo;
import com.spring_graphQL.spring_graphQL.model.Person;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.DataFetcher;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.List;

@RestController
public class PersonController {
    @Value("classpath:person.graphqls")
    private Resource schemaResource;
    @Autowired
    PersonRepo personRepo;

    GraphQL graphQL;

    @PostConstruct
    public void loadSchema() throws IOException {
        File schemaFile = schemaResource.getFile();
        TypeDefinitionRegistry registry = new SchemaParser().parse(schemaFile);
        RuntimeWiring runtimeWiring = buildRunTimeWiring();
        GraphQLSchema graphQLSchema=new SchemaGenerator().makeExecutableSchema(registry,runtimeWiring);
        graphQL=GraphQL.newGraphQL(graphQLSchema).build();
    }

    private RuntimeWiring buildRunTimeWiring() {
        DataFetcher<List<Person>> fetcher1 = data -> {
            return (List<Person>) personRepo.findAll();
        };
        DataFetcher<Person> fetcher2 = data -> {
            return personRepo.findByEmail(data.getArgument("email"));
        };
        return RuntimeWiring.newRuntimeWiring().type("Query", typeWriting ->
            typeWriting.dataFetcher("getAllPersons", fetcher1).dataFetcher("findPerson", fetcher2)
        ).build();
    }

    @PostMapping("/addPersons")
    public String addPersons(@RequestBody List<Person> person) {
        personRepo.saveAll(person);
        return "Record Insterted " + person.size();
    }

    @GetMapping("/findPersons")
    public List<Person> findAllPersons() {
        return (List<Person>) personRepo.findAll();
    }
    @PostMapping("/getALl")
    public ResponseEntity<Object> getAllPersons(@RequestBody String query){
        ExecutionResult result=graphQL.execute(query);
        return new ResponseEntity<Object>(result, HttpStatus.OK);
    }
    @PostMapping("/getByEmail")
    public ResponseEntity<Object> getByEmail(@RequestBody String query){
        ExecutionResult result=graphQL.execute(query);
        return new ResponseEntity<>(result,HttpStatus.OK);
    }
}
