package com.spring_graphQL.spring_graphQL.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class Person {
    @Id
    private int id;
    private String name;
    private String email;
    private  String mobile;
    private String[] address;
}
