package com.spring_graphQL.spring_graphQL.dao;

import com.spring_graphQL.spring_graphQL.model.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepo extends CrudRepository<Person,Integer> {
     Person findByEmail(String email);
}
